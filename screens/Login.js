import React from "react";
import axios from 'axios';
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";

import { Button, Icon, Input,BgTracking } from "../components";
import { Images, argonTheme } from "../constants";
import { AsyncStorage } from 'react-native';
const { width, height } = Dimensions.get("screen");

class Login extends React.Component {

constructor(){
  super();
  this.checkToken();
}

      state = {
        email: '',
        password: '',
        loading: false
    };


checkToken = async () =>{
  const token = await AsyncStorage.getItem("token");

  if(token){
    this.props.navigation.navigate('App');
  }
}




handleClick = () => {

const req = {
  email : this.state.email,
  password:this.state.password
}

this.setState({
  loading:true
})


 axios.post(`https://ipmanagment.com.au/api/user`,req)
      .then(
        res => {
          this.setState({
            loading:false
          }),

          console.warn(res.data);
     
     if(res.data.token == '404'){
         this.setState({
            loading:false
          }),
       alert('Please Check Email and Password');
     }
     else {
        AsyncStorage.setItem('token', res.data.token)
      .then(
        this.props.navigation.navigate('App')
      );
     }
            
  }


      )}

  async getApiData()
  {
    axios.get('https://jsonplaceholder.typicode.com/todos/')
      .then(function (response) {
    // handle success
    console.warn(response);
  })
  .catch(function (error) {
    // handle error
    console.warn(error);
  })
  .then(function () {
    // always executed
  });

  }
  render() {
    const { username , password, loading} = this.state;
    return (
      <Block flex middle>
        <StatusBar hidden />
     
          <Block safe flex middle>
            <Block style={styles.loginContainer}>
              
              <Block flex>
                
                <Block flex center>
                  <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior="padding"
                    enabled
                  >
                    
                    <Block width={width * 0.8} style={{ marginBottom: 15,marginTop:25 }}>
                      <Input
                        borderless
                        placeholder="Email"
                         onChangeText={(value) => this.setState({email: value})}
                          value={this.state.email}
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="ic_mail_24px"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                    </Block>
                    <Block width={width * 0.8}>
                      <Input
                        password
                        borderless
                        placeholder="Password"
                        onChangeText={(value) => this.setState({password: value})}
                          value={this.state.password}
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="padlock-unlocked"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                    
                    </Block>
                    
                    <Block middle>
                      <Button onPress={this.handleClick}  color="primary" style={{...styles.createButton,backgroundColor:loading ? "#ddd" : "blue"}} disabled={loading}>
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          {loading ? 'Loading...':"Sign In"}
                        </Text>
                      </Button>
                    </Block>
                  </KeyboardAvoidingView>
                </Block>
              </Block>
            </Block>
          </Block>
      {/* <BgTracking /> */}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  loginContainer: {
    width: width * 0.9,
    height: height * 0.875,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default Login;
