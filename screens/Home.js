import React, { useState, useEffect, Component } from 'react';
import axios from 'axios';

import { Platform, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Block, theme } from 'galio-framework';

import { Card } from '../components';
import articles from '../constants/articles';
const { width } = Dimensions.get('screen');

import { Text, View,TouchableOpacity  } from 'react-native';
import { AsyncStorage } from 'react-native';
import * as TaskManager from 'expo-task-manager';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';



const LOCATION_TASK_NAME = 'background-location-task';
const token = AsyncStorage.getItem("token");


export default function App() {
  
const [gpsLatitude, setgpsLatitude] = useState(null);
const [gpsLongitude, setgpsLongitude] = useState(null);
const [batteryLevel, setBatteryLevel] = useState(null);

  useEffect(() => {
    (async () => await _askForLocationPermission())();

    const interval = setInterval(() => {
      uploadDataAtInterval();
    }, 10000);
    return () => clearInterval(interval);
  });

const backgroundLocationFetch = async () => {
    const { status } = await Location.requestPermissionsAsync();
    if (status === 'granted') {
      console.warn('cmon dance with me!')
      await Location.startLocationUpdatesAsync('FetchLocationInBackground', {
        accuracy: Location.Accuracy.Balanced,
        timeInterval: 10000,
        distanceInterval: 1,
        foregroundService: {
          notificationTitle: 'Live Tracker',
          notificationBody: 'Live Tracker is on.'
        }
      });
    }
  }

 const _askForLocationPermission = async () => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        setgpsErrorMsg('Permission to access location was denied');
      }
    })();
  };


  const uploadDataAtInterval = async () => {

    getGPSPosition();
      const req = {
      latitude : gpsLatitude,
      longitude:gpsLongitude,
      user_id:token['_W']
    }


    axios.post(`https://ipmanagment.com.au/api/location`,req)
          .then(
            res => {
            console.warn('Saved');
      })
  }

  const getGPSPosition = async () => {
    let location = await Location.getCurrentPositionAsync({accuracy:Location.Accuracy.High});
    setgpsLatitude(location.coords['latitude']);
    setgpsLongitude(location.coords['longitude']);

  }

  backgroundLocationFetch();

  return (
     <TouchableOpacity onPress={backgroundLocationFetch}>
    <Text>Enable background location</Text>
  </TouchableOpacity>
  );


}


TaskManager.defineTask('FetchLocationInBackground', ({ data, error }) => {
  if (error) {
    console.log("Error bg", error)

      const req = {
      latitude : '111',
      longitude:'222',
      user_id:'333'
    }


    axios.post(`https://ipmanagment.com.au/api/location`,req)
          .then(
            res => {
            console.warn('Saved');
      })
  

   
  }
  if (data) {
    const { locations } = data;
    console.log("BGGGG->", locations[0].coords.latitude, locations[0].coords.longitude);


      const req = {
      latitude :locations[0].coords.latitude,
      longitude:locations[0].coords.longitude,
      user_id:'333312'
    }

       axios.post(`https://ipmanagment.com.au/api/location`,req)
          .then(
            res => {
            
      })
  }
});